# SISTEM INFORMASI ADMINISTRASI MAHASISWA

> Insert, Update, Delete and View
,`database`,`query`,`mysql`,`html`

Skill dasar hingga menengah untuk php dan mysql telah didapatkan, nah sekarang
bagaimana mengimplementasi skill yang telah kita dapatkan sebelumnya. Oke, kita akan
membuat sistem informasi Administrasi Mahasiswa.

### Sistem Informasi Administrasi ini memiliki fitur antara lain :
```
1. Form Pendaftaran Mahasiswa (FrontEnd)
```
```
2. Daftar Calon Mahasiswa Baru (BackEnd)
```
```
3. Form Pengisian Nilai Mahasiswa (BackEnd)
```
```
4. Daftar Nilai Mahasiswa (BackEnd)
```
```
5. Fitur News/Article Update Ala Blog (Frontend BackEnd)
```

```
6. Fitur Comment Article Ala Blog (FrontEnd)
```


`FrontEnd disini berarti, di peruntukkan untuk user/publik saja yang bisa melihat.
Sedangkan BackEnd disini berarti, hanya si admin saja yang dapat mengakses, termasuk
didalamnya menambah, melihat, mengedit, ataupun menghapus. (CRUD) Create, Read,
Update, Delete.`


`PROJECT DIKIRIM KE SALAH SATU DIBAWAH INI`
`Syaiful9507@gmail.com`
`java.pens@gmail.com`